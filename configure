#!/bin/sh

. package/info

usage () {
cat <<EOF
Usage: $0 [OPTION]... [TARGET]

Defaults for the options are specified in brackets.

Installation directories:
  --prefix=PREFIX                  main installation prefix [/]
  --exec-prefix=EPREFIX            installation prefix for executable files [PREFIX]

Fine tuning of the installation directories:
  --bindir=BINDIR                  user executables [EPREFIX/bin]
  --libdir=LIBDIR                  static library files [PREFIX/lib/$package_macro_dir]
  --sysconfdir=SYSDIR              global configuration files [/etc]
  --datarootdir=DATAROOTDIR        read-only architecture-independent data root [PREFIX/share]
  --mandir=DIR                     man documentation [$datarootdir/man]
  --livedir=DIR                    66 default live directory [/run/66]
  --with-system-service=DIR        66 service intallation directory [DATAROOTDIR/$package_macro_dir/service]
  --with-system-script=DIR         66 script installation directory of scripts [DATAROOTDIR/$package_macro_dir/script]
  --with-system-seed=DIR           system trees default configuration[DATAROOTDIR/$package_macro_dir/seed]
  --with-skeleton=DIR              66 skeleton files directory installation [SYSDIR/$package_macro_dir]
  --shebangdir=DIR                 absolute path for execline #\! invocations [BINDIR]

Fine tunning of boot configuration:
  --version=VALUE                  version of the service[0.0.1]

EOF
exit 0
}

# Helper functions

# If your system does not have printf, you can comment this, but it is
# generally not a good idea to use echo.
# See http://www.etalabs.net/sh_tricks.html
echo () {
  IFS=" "
  printf %s\\n "$*"
}

quote () {
  tr '\n' ' ' <<EOF | grep '^[-[:alnum:]_=,./:]* $' >/dev/null 2>&1 && { echo "$1" ; return 0 ; }
$1
EOF
  echo "$1" | sed -e "s/'/'\\\\''/g" -e "1s/^/'/" -e "\$s/\$/'/" -e "s#^'\([-[:alnum:]_,./:]*\)=\(.*\)\$#\1='\2#" -e "s|\*/|* /|g"
}

fail () {
  echo "$*"
  exit 1
}

fnmatch () {
  eval "case \"\$2\" in $1) return 0 ;; *) return 1 ;; esac"
}

cmdexists () {
  type "$1" >/dev/null 2>&1
}

stripdir () {
  while eval "fnmatch '*/' \"\${$1}\"" ; do
    eval "$1=\${$1%/}"
  done
}

testval () {
 if test -z ${1}; then
    return 0 ;
 else
    return 1 ;
 fi
}

# Actual script

prefix=
exec_prefix='$prefix'
bindir='$exec_prefix/bin'
libdir='$prefix/lib/$package_macro_dir'
sysconfdir='/etc'
datarootdir='$prefix/share'
mandir='$datarootdir/man'
livedir='/run/66'
service_directory='$datarootdir/$package_macro_dir/service'
script_directory='$datarootdir/$package_macro_dir/script'
seed_directory='$datarootdir/$package_macro_dir/seed'
adm_conf='$sysconfdir/$package_macro_dir/conf'
skel_directory='$sysconfdir/$package_macro_dir'
shebangdir='$bindir'
version='0.0.1'

for arg ; do
  case "$arg" in
    --help) usage ;;
    --prefix=*) prefix=${arg#*=} ;;
    --exec-prefix=*) exec_prefix=${arg#*=} ;;
    --bindir=*) bindir=${arg#*=} ;;
    --libdir=*) libdir=${arg#*=} ;;
    --sysconfdir=*) sysconfdir=${arg#*=} ;;
    --datarootdir=*) datarootdir=${arg#*=} ;;
    --mandir=*) mandir=${arg#*=} ;;
    --livedir=*) livedir=${arg#*=} ;;
    --with-system-service=*) service_directory=${arg#*=} ;;
    --with-system-script=*) script_directory=${arg#*=} ;;
    --with-system-seed=*) seed_directory=${arg#*=} ;;
    --with-sysadmin-service-conf=*) adm_conf=${arg#*=} ;;
    --with-skeleton=*) skel_directory=${arg#*=} ;;
    --shebangdir=*) shebangdir=${arg#*=} ;;
    --version=*) version=${arg#*=} ;;
    -* ) echo "$0: unknown option $arg" ;;
    *=*) ;;
    *) target=$arg ;;
  esac
done

# Add /usr in the default case
if test -z "$prefix" ; then
  if test "$libdir" = '$prefix/lib/$package_macro_dir' ; then
    libdir=/usr/lib/$package_macro_dir
  fi
  if test "$datarootdir" = '$prefix/share'; then
    datarootdir=/usr/share
  fi
fi

if test -z "$sysconfdir" ; then
  if test "$sysconfdir" = '$sysconfdir' ; then
    sysconfdir = '/etc'
  fi
  if test "$adm_conf" = '$sysconfdir/$package_macro_dir/conf' ; then
    adm_conf = '$sysconfdir/$package_macro_dir/conf'
  fi
  if test "$skel_directory" = '$sysconfdir/$package_macro_dir' ; then
    skel_directory = '$sysconfdir/$package_macro_dir'
  fi
fi

# Expand installation directories
stripdir prefix
for i in exec_prefix bindir libdir sysconfdir datarootdir mandir livedir \
        service_directory script_directory seed_directory skel_directory adm_conf shebangdir; do
  eval tmp=\${$i}
  eval $i=$tmp
  stripdir $i
done

stripdir sysconfdir
for i in adm_conf skel_directory; do
  eval tmp=\${$i}
  eval $i=$tmp
  stripdir $i
done

stripdir datarootdir
for i in service_directory script_directory seed_directory mandir; do
  eval tmp=\${$i}
  eval $i=$tmp
  stripdir $i
done

# Get usable temp filenames
i=0
set -C
while : ; do
  i=$(($i+1))
  tmpc="./tmp-configure-$$-$PPID-$i.c"
  tmpe="./tmp-configure-$$-$PPID-$i.tmp"
  2>|/dev/null > "$tmpc" && break
  2>|/dev/null > "$tmpe" && break
  test "$i" -gt 50 && fail "$0: cannot create temporary files"
done
set +C
trap 'rm -f "$tmpc" "$tmpe"' EXIT ABRT INT QUIT TERM HUP

echo "creating config.mak..."
cmdline=$(quote "$0")
for i ; do cmdline="$cmdline $(quote "$i")" ; done
exec 3>&1 1>config.mak
cat << EOF
# This file was generated by:
# $cmdline
# Any changes made here will be lost if configure is re-run.
version := $version
prefix := $prefix
exec_prefix := $exec_prefix
bindir := $bindir
libdir := $libdir
sysconfdir := $sysconfdir
datarootdir := $datarootdir
mandir := $mandir
livedir := $livedir
service_directory := $service_directory
script_directory := $script_directory
seed_directory := $seed_directory
adm_conf := $adm_conf
skel_directory := $skel_directory
package_macro_dir := $package_macro_dir
shebangdir := $shebangdir
version := $version
EOF

exec 1>&3 3>&-
echo "  ... done."
